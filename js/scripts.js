// events on load
window.onload = function() {
	openMenu();
	fullscreenSlider();
	testimonialsSlider();
}



// events on scroll
$(window).scroll(function() {
	fixedHeader();
});



// events on resize
window.onresize = function() {
	closeMenu();
	fixedHeader();
}



// function declarations
function openMenu() {
	$('.button').click(function(){
		if ($('body').hasClass('menu-is-open')) {
			$('body').removeClass('menu-is-open');
		}
		else {
			$('body').addClass('menu-is-open');
		}
	});
}

function fixedHeader() {
	var scrollPosition = $(window).scrollTop();
	var fixedHeaderMenu = $('.site-header');
	var fixedStartPosition = 80;
	if ( (scrollPosition >= fixedStartPosition) && ($(window).width() > '1200')) {
		fixedHeaderMenu.addClass("is-fixed");
	} else {
		fixedHeaderMenu.removeClass("is-fixed");
	}	
}

function closeMenu() {
	if ($(window).width() > '1199') {
		$('body').removeClass('menu-is-open');
	}
}

function fullscreenSlider() {
	$('.fullscreen-slider').owlCarousel({
		slideSpeed : 300,
		paginationSpeed : 400,
		singleItem: true,
		mouseDrag: true,
		touchDrag: true,
		autoPlay: 10000,
		pagination: false,
	});
}


function testimonialsSlider() {
	var owl = $('.testimonials-slides');
	owl.owlCarousel({
		pagination : false,
		slideSpeed : 300,
		paginationSpeed : 400,
		singleItem: true,
		mouseDrag: true,
		touchDrag: false,
		autoPlay: 5000,
		afterAction : afterAction
	});

	function afterAction() {$('.control-indicator--current').text(this.owl.currentItem+1);}
	var totalSlides = $('.testimonials-slides').find('.testimonials-item').length;
	$('.control-indicator--total').text(totalSlides);
	$('.control-next').click(function() {owl.trigger('owl.next');})
	$('.control-prev').click(function() {owl.trigger('owl.prev');})
}